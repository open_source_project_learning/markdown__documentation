# 1.换行
方式一：在末尾添加两个空格  
方式二：使用两个换行


# 2.标题
使用 # 开头来表示一个标题，# 的个数表示标题的级数
# 一级标题
## 二级标题
### 三级标题
#### 四级标题
##### 五级标题
###### 六级标题

# 3.字体
 **加粗**  
 _倾斜_  
 ~~删除线~~  
`标签`

# 4.其他
### 分割线
***
### 链接
* [必应](http://www.bing.com)

### 图片
![输入图片说明](https://images.gitee.com/uploads/images/2020/0730/150612_5122d2dd_1642418.gif "在这里输入图片标题")

### 表格
|  魏 | 蜀  | 吴  |
|---|---|---|
|  曹操 | 刘备  | 孙坚  |
| 司马懿  | 诸葛亮  | 周瑜  |


<div>
        <table border="0">
	  <tr>
	    <th>书籍</th>
	    <th>作者</th>
	  </tr>
	  <tr>
	    <td>西游记</td>
	    <td>（明）吴承恩</td>
	  </tr>
	</table>
</div>


### 代码

```
Logger log = LoggerFactory.getLogger(Thread.class);
```


### 区块引用
> 国学经典
>
> > 四大名著
>
>  > > 红楼梦
>
> > > > 曹雪芹


### 列表
* 关羽
* 张飞
* 赵云

### 转义
\\ 使用反斜杠进行转义  

### 公式
$xyz$  

$$xyz$$  

$x^4$  

$x_1$  

${16}_{8}O{2+}_{2}$  

$V_{\mbox{初始}}$  

$\displaystyle \frac{x+y}{y+z}$  

$\underline{x+y}$  

$\tag{11}$  

$\overbrace{a+b+c+d}^{2.0}$  

$a+\underbrace{b+c}_{1.0}+d$  

$\vec{x}\stackrel{\mathrm{def}}{=}{x_1,\dots,x_n}$  


### 资料：
* https://www.jianshu.com/p/61e02a55f2a6

